#!/bin/bash

#############################################################
# Submitting a GRID job
# Goals: check GRID/cvmfs environment and software stack etc.
#############################################################

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo "${DIR}"

JOBNAME=GRIDTESTNIGHTLY

for i in 1 2 3 4; do
  NEVENTS=50 ${DIR}/gridjob_template.sh ${JOBNAME}${i}
  # query PROCESS_ID
done
