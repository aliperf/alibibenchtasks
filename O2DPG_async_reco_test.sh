#!/bin/bash

#
# This is running an async reco setup by utilizing part of the anchoring MC
# script/setup

# get a CTF input_file
export CTF_TEST_FILE="o2_ctf_run00505673_orbit0000035596_tf0000000037.root"
alien.py cp /alice/data/2021/OCT/505673/raw/0850/${CTF_TEST_FILE} file:./

# run the async reco (pass4) and switch off MC
NO_MC=ON NTIMEFRAMES=2 ${O2DPG_ROOT}/MC/run/ANCHOR/2021/OCT/pass4/anchorMC.sh

# If possible run QC and analysis on top
MCRC=$?

if [ "${MCRC}" = "0" ]; then

  # QC was done in anchor, only check if successful
  err_logs=$(get_error_logs $(pwd) --include-grep "QC")
  [ "${err_logs}" != "" ] && send_mattermost "--text QC stage **failed** :x: --files ${err_logs}" || send_mattermost "--text QC **passed** :white_check_mark:"

  # The O2DPG async script

  unset ALICEO2_CCDB_LOCALCACHE
fi

# publish the original data to ALIEN
tar -czf mcarchive.tar.gz workflow.json tf* QC pipeline*
copy_ALIEN mcarchive.tar.gz

return ${MCRC} 2> /dev/null || exit ${MCRC}
