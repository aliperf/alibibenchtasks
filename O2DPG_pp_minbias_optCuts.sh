#!/bin/bash

#
# A example workflow MC->RECO->AOD for a simple pp min bias with optimised medium cuts
# production

# make sure O2DPG + O2 is loaded
[ ! "${O2DPG_ROOT}" ] && echo "Error: This needs O2DPG loaded" && exit 1
[ ! "${O2_ROOT}" ] && echo "Error: This needs O2 loaded" && exit 1

# Store the directory where this script lives
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# ----------- SETUP LOCAL CCDB CACHE --------------------------
export ALICEO2_CCDB_LOCALCACHE=$PWD/.ccdb

# ----------- START ACTUAL JOB  -----------------------------

NWORKERS=${NWORKERS:-8}
MODULES="--skipModules ZDC"
SIMENGINE=${SIMENGINE:-TGeant4}


get_from_test_CCDB -o cut_file.json -u SIM_TEST/ALIBI/SIM_CUTS
MCRC=$?
if [[ "${MCRC}" != "0" ]] ; then
  send_mattermost "--text Cannot find cuts on CCDB for current timestamp"
else
  cut_file_path=$(realpath cut_file.json)
  # create workflow
  # run number 302000 is ok for pp and -0.5 T
  ${O2DPG_ROOT}/MC/bin/o2dpg_sim_workflow.py -eCM 14000  -col pp -gen pythia8 -proc cdiff -tf 5     \
                                                         -ns 2000 -e ${SIMENGINE}                   \
                                                         -j ${NWORKERS} -interactionRate 500000     \
                                                         -run 302000 -seed 624                     \
                                                         -confKey "MaterialManagerParam.inputFile=${cut_file_path}" --include-qc --include-analysis \
                                                         -productionTag "alibi_O2DPG_pp_minbias"

  # run workflow
  ${O2DPG_ROOT}/MC/bin/o2_dpg_workflow_runner.py -f workflow.json -tt aod
  MCRC=$?

  if [ "${MCRC}" = "0" ]; then

    # do QC tasks
    ${O2DPG_ROOT}/MC/bin/o2_dpg_workflow_runner.py -f workflow.json --target-labels QC --cpu-limit 32
    RC=$?
    err_logs=$(get_error_logs $(pwd) --include-grep "QC")
    [ ! "${RC}" -eq 0 ] && send_mattermost "--text QC stage **failed** :x: --files ${err_logs}" || send_mattermost "--text QC **passed** :white_check_mark:"
  fi

fi
return ${MCRC} 2> /dev/null || exit ${MCRC}
