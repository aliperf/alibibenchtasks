#!/bin/bash

#
# A example workflow MC->RECO->AOD for a simple pp min bias
# production

# make sure O2DPG + O2 is loaded
[ ! "${O2DPG_ROOT}" ] && echo "Error: This needs O2DPG loaded" && exit 1
[ ! "${O2_ROOT}" ] && echo "Error: This needs O2 loaded" && exit 1

# ----------- START ACTUAL JOB  -----------------------------

NWORKERS=${NWORKERS:-8}
MODULES="--skipModules ZDC"
SIMENGINE=${SIMENGINE:-TGeant4}

# create workflow
# run number 302000 is ok for pp and -0.5 T
${O2DPG_ROOT}/MC/bin/o2dpg_sim_workflow.py -eCM 14000  -col pp -gen pythia8 -proc cdiff -tf 5     \
                                                       -ns 2000 -e ${SIMENGINE}                   \
                                                       -j ${NWORKERS} -interactionRate 500000     \
                                                       -run 302000 -seed 624                      \
                                                       --include-qc --include-analysis            \
                                                       -productionTag "alibi_O2DPG_pp_minbias"

# run workflow
${O2DPG_ROOT}/MC/bin/o2_dpg_workflow_runner.py -f workflow.json -tt aod --cpu-limit 32
MCRC=$?

if [ "${MCRC}" = "0" ]; then

  # do QC tasks
  ${O2DPG_ROOT}/MC/bin/o2_dpg_workflow_runner.py -f workflow.json --target-labels QC --cpu-limit 32
  RC=$?
  err_logs=$(get_error_logs $(pwd) --include-grep "QC")
  if [ ! "${RC}" -eq 0 ] ; then
    send_mattermost "--text QC stage **failed** :x: --files ${err_logs}"
  else
    send_mattermost "--text QC **passed** :white_check_mark:"
  fi
fi

return ${MCRC} 2> /dev/null || exit ${MCRC}
