# Performs a list of analysis unit tests
# and reports success/failure.
# Should be sourced into concrete simulation nightly tasks

for t in ${ANATESTLIST:-$(${O2DPG_ROOT}/MC/analysis_testing/o2dpg_analysis_test_config.py show-tasks --enabled)}; do
  ${O2DPG_ROOT}/MC/analysis_testing/analysis_test.sh --analysis ${t} --aod AO2D.root
done
