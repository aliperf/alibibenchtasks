Repository with benchmark tasks to run on ALICE alibi infrastructure.
The simplified cron file describes which tasks are to be run which day.

These tasks will be executed by a master task running 
nightly on the infrastructure.

Some documentation on the infrastructure is available here: https://alisw.github.io/infrastructure-alibi-user-guide
