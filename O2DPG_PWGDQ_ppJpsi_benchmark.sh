#!/bin/bash

# Performing a PWGDQ pp embedding example for the reference beam
# Embedding is done with spacing of signal events

RNDSEED=${RNDSEED:-0}
NSIGEVENTS=${NSIGEVENTS:-201}
NBKGEVENTS=${NBKGEVENTS:-2400}
NWORKERS=${NWORKERS:-8}
NTIMEFRAMES=${NTIMEFRAMES:-20}

# ----------- SETUP LOCAL CCDB CACHE --------------------------
export ALICEO2_CCDB_LOCALCACHE=$PWD/.ccdb


${O2DPG_ROOT}/MC/bin/o2dpg_sim_workflow.py -eCM 12000 -gen external -j ${NWORKERS} -ns ${NSIGEVENTS} -tf ${NTIMEFRAMES} -e TGeant4 \
    -mod "--skipModules ZDC" \
    -confKey "GeneratorExternal.fileName=${O2DPG_ROOT}/MC/config/PWGDQ/external/generator/GeneratorParamPromptJpsiToElectronEvtGen_pp13TeV.C;GeneratorExternal.funcName=GeneratorParamPromptJpsiToElectronEvtGen_pp13TeV()".                \
    -genBkg pythia8 -procBkg inel -colBkg pp --embedding -nb ${NBKGEVENTS}                     \
    -interactionRate 500000                                                                    \
    --embeddPattern '@0:e12' --include-analysis -productionTag "alibi_O2DPG_PWGDQ_ppJpsi_benchmark" -run 303000

# run workflow (highly-parallel)
time ${O2DPG_ROOT}/MC/bin/o2_dpg_workflow_runner.py -f workflow.json -t aod --cgroup 8coregrid

MCRC=$?

return ${MCRC} 2> /dev/null || exit ${MCRC}
