#!/bin/bash

#
# A example workflow MC->RECO->AOD for a simple pp min bias
# production, targetting test beam conditions.
#
# In addition, we target to exercise the whole chain of anchoring
# mechanisms, including
# - transfer settings from DATA reconstruction pass scripts
# - anchor to the time of a specific data dating run, so that
#   correct CCDB are fetched
# - apply additional settings like vertex/beam spot etc., not yet coming
#   from elsewhere

# this is to find the helper functions gen_topo_helper_functions.sh (TODO fix inside O2DPG script)

cp ${O2DPG_ROOT}/DATA/production/gen_topo*.sh .
NTIMEFRAMES=2 ${O2DPG_ROOT}/MC/run/ANCHOR/2021/OCT/pass4/anchorMC.sh

# If possible run QC and analysis on top
MCRC=$?

if [ "${MCRC}" = "0" ]; then

  # QC was done in anchor, only check if successful
  err_logs=$(get_error_logs $(pwd) --include-grep "QC")
  [ "${err_logs}" != "" ] && send_mattermost "--text QC stage **failed** :x: --files ${err_logs}" || send_mattermost "--text QC **passed** :white_check_mark:"
fi

# publish the original data to ALIEN
tar -czf mcarchive.tar.gz workflow.json tf* QC pipeline*
copy_ALIEN mcarchive.tar.gz

return ${MCRC} 2> /dev/null || exit ${MCRC}
