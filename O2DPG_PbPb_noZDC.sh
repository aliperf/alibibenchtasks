#!/bin/bash

#
# A example workflow MC->RECO->AOD for a simple PbPb production
# excluding ZDC

# make sure O2DPG + O2 is loaded
[ ! "${O2DPG_ROOT}" ] && echo "Error: This needs O2DPG loaded" && exit 1
[ ! "${O2_ROOT}" ] && echo "Error: This needs O2 loaded" && exit 1

# ----------- START ACTUAL JOB  -----------------------------
NWORKERS=${NWORKERS:-16}
SIMENGINE=${SIMENGINE:-TGeant4}

# create workflow
${O2DPG_ROOT}/MC/bin/o2dpg_sim_workflow.py -eCM 5020 -col PbPb -gen pythia8 -proc "heavy_ion" -tf 10 \
                                                     -ns 25 -e ${SIMENGINE} -j ${NWORKERS}           \
                                                     -run 310000 -seed 624 -interactionRate 50000

# run workflow
${O2DPG_ROOT}/MC/bin/o2_dpg_workflow_runner.py -f workflow.json -tt aod --cpu-limit 32
MCRC=$?

return ${MCRC} 2> /dev/null || exit ${MCRC}
