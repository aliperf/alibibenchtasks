#!/bin/bash

# record environment
export > full_system_test.env

#performing a smaller sized full system test

# provide the required binary objects
DATAPATH=data_cache/full-system-test/
cp ${ALIPERF_DATA_DIR}/${DATAPATH}/matbud.root .
cp ${ALIPERF_DATA_DIR}/${DATAPATH}/ITSdictionary.bin .

export O2COMMIT=${ALIPERF_O2COMMIT}
export ALIDISTCOMMIT=${ALIPERF_ALIDISTCOMMIT}

# call the actual script template
O2SIMSEED=1234 ENABLE_GPU_TEST=0 NEvents=50 NEventsQED=3000 $O2_ROOT/prodtests/full_system_test.sh

# acceptance check:
CHECK=1
[ -f "reco_NOGPU.log_done" ] && CHECK=0  # the reco task should complete


echo "Check code ${CHECK}"

# submit metrics file to monitoring system
submit_to_AliPerf_InfluxDB metrics.dat


return ${CHECK} 2>/dev/null || exit ${CHECK}
