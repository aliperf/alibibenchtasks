#!/bin/bash
# This is a simple check for execution of HEPscore benchmark
# against CCDB cache

# version to check (the last nightly)
O2SIM_PACKAGE_LATEST=`find /cvmfs/alice.cern.ch/el7-x86_64/Modules/modulefiles/O2sim -name "v20*" -type f -printf "%f\n" | tail -n1`

# create command
COMMAND="export; eval \"\$(/cvmfs/alice.cern.ch/bin/alienv printenv O2sim::\"${O2SIM_PACKAGE_LATEST}\")\""
# this sets the CCDB cache
COMMAND="${COMMAND}; env; export HEPSCORE_CCDB_ROOT=/cvmfs/alice.cern.ch/el7-x86_64/Packages/HEPscore-CCDB/v0.1-test-1"
# this is a tmp bugfix
COMMAND="${COMMAND}; export ROOT_INCLUDE_PATH=\${ROOT_INCLUDE_PATH}:/cvmfs/alice.cern.ch/el7-x86_64/Packages/TBB/v2021.5.0-8/include"
COMMAND="${COMMAND}; NBKGEVENTS=1 NSIGEVENTS=1 \${O2DPG_ROOT}/MC/run/HEPscore/hep-score.sh "
echo ${COMMAND} > ./container_script.sh

# execute in a container with clean environment and no network interface
# singularity exec --nonet -C -B /cvmfs:/cvmfs,${PWD}:/workdir --env JALIEN_TOKEN_CERT="foo" --pwd /workdir /cvmfs/alice.cern.ch/containers/fs/singularity/centos7 bash ./singscript.sh
docker run -itv $PWD:/workdir -v /cvmfs/:/cvmfs --net none \
           -u $(id -u ${USER}):$(id -g ${USER})            \
           --shm-size=8gb -w /workdir alisw/slc8-gpu-builder /bin/bash ./container_script.sh

# analyse return code
RETURN_CODE=$?
[ ! "${RETURN_CODE}" -eq 0 ] && echo "test failed"

# return with this erro code (or exit)                       
return ${RETURN_CODE} 2>/dev/null || exit ${RETURN_CODE}
