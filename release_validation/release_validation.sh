#!/bin/bash

# Perform RelVal centrally for a given task

get_analysis_name()
{
  # Get the analysis name from full path
  local root_path=$(realpath ${1})
  local analysis_name=${root_path##*Analysis/}
  analysis_name=${analysis_name%%/$(basename ${root_path})}
  echo ${analysis_name}
}

get_qc_name()
{
  # Get the QC name from full path
  local root_path=$(realpath ${1})
  local qc_name=${root_path##*QC/}
  qc_name=${qc_name%%.root}
  echo ${qc_name}
}

# Assume we are in a simulation directory
THIS_DIR=$(pwd)
TASK_NAME=$(get_task_name_from_path ${THIS_DIR})
for t in ${RELVAL_DIRS:-Analysis QC}; do

  test_dir=${THIS_DIR}/${t}
  [[ ! -d ${test_dir} ]] && continue

  root_files=$(find ${test_dir} -type f -name "*.root")
  for rf in ${root_files} ; do
    [[ "${t}" == "Analysis" ]] && identifier=${t}_$(get_analysis_name ${rf}) || identifier=${t}_$(get_qc_name ${rf})
    release_validation_for_file ${rf} --to-influx --identifier ${identifier}
    # If minbias_pp, check if compatible to minbias_pp_optCuts
    [[ "${TASK_NAME}" == "O2DPG_pp_minbias" ]] && release_validation_for_file ${rf} --to-influx --identifier ${identifier} --compare-task "O2DPG_pp_minbias_optCuts"
  done

done
