#!/bin/bash

# Perform RelVal for detectors for a given subset of their QC objects

make_exact_match_patterns()
{
  # add "$" to object names given as input
  local new_names=
  for n in ${@} ; do
    new_names+="${n}\$ "
  done
  echo ${new_names}
}


# run on these tasks
RUN_ON_TASKS=${RUN_ON_TASKS:-O2DPG_pp_minbias}
# run for tehse detectors
DETECTORS=${DETECTORS:-ITS TPC TOF TRD MFT MCH MID HMP EMC PHS CPV FT0 FV0 FDD ZDC}
# log results here
LOG_FILE=${LOG_FILE:-detectors_qc_rel_val.log}

# Assume we are in a simulation directory
THIS_DIR=$(pwd)
# this is to normalise this directory to really have the top directory of the task/simulation
THIS_DIR=$(get_sim_path ${THIS_DIR})
# derive the task name
TASK_NAME=$(get_task_name_from_path ${THIS_DIR})

# determine if to be run for this TASK_NAME
RUN=
for t in ${RUN_ON_TASKS} ; do
  [[ "${t}" == ${TASK_NAME} ]] && { RUN=1 ; break ; }
done

[[ -z ${RUN} ]] && { echo "Task ${TASK_NAME} not selected for detector RelVal." ; exit 0 ; }

[[ "$(find ${THIS_DIR}/QC -name "*.root" 2>/dev/null)" == "" ]] && { echo "No ROOT files found in ${THIS_DIR}/QC}" ; exit 0 ; }

send_mattermost "--text :face_with_monocle: Running detector-specific RelVal for asyncQC for task ${TASK_NAME}"
send_mattermost_qc_relval "--text :face_with_monocle: Running detector-specific RelVal for asyncQC for task ${TASK_NAME}"

PREVIOUS_DIR=
# browse for the paths of the same task that have been run previously
for d in $(get_test_dirs_for_task ${TASK_NAME}) ; do
  # continue if same directory
  [[ "${d}" == "${THIS_DIR}" ]] && continue
  # if no ROOT files in QC directory, continue
  [[ "$(find ${d}/QC -name "*.root" 2>/dev/null)" == "" ]] && continue
  PREVIOUS_DIR=${d}
  break
done

if [[ -z ${PREVIOUS_DIR} ]] ; then
    echo "Cannot find QC directory for a previously done task ${TASK_NAME}."
    send_mattermost "--text Cannot find QC directory for a previously done task ${TASK_NAME}."
    send_mattermost_qc_relval "--text Cannot find QC directory for a previously done task ${TASK_NAME}."
    exit 0
fi

# main outout directory
OUTDIR=${THIS_DIR}/QC_rel_val

# write to logfile
echo "DIR=${OUTDIR}" > ${LOG_FILE}
# make a header (basically declaring the columns) in the log file
echo "#DETECTOR;RELVAL_DIR;BAD;CRIT_NC" >> ${LOG_FILE}

# do RelVal for everything once, do not plot at this point, only extract numbers, label with dates/time
${O2DPG_ROOT}/RelVal/o2dpg_release_validation.py rel-val -i ${THIS_DIR}/QC/*.root -j ${PREVIOUS_DIR}/QC/*.root --no-plot --labels $(get_meta_info ${THIS_DIR} "time") $(get_meta_info ${PREVIOUS_DIR} "time") --output ${OUTDIR}

for d in ${DETECTORS} ; do
    # if there is a pattern file for this detector, use it, otherwise continue
    pattern_file=${O2DPG_ROOT}/RelVal/config/QC/async/${d}_include_patterns_default.txt
    [[ ! -f ${pattern_file} ]] && continue
    has_bad=$(${O2DPG_ROOT}/RelVal/o2dpg_release_validation.py print --path ${OUTDIR} --interpretations BAD --object-names --include-patterns @${pattern_file} | tr '\n' ' ')
    has_non_comparable=$(${O2DPG_ROOT}/RelVal/o2dpg_release_validation.py print --path ${OUTDIR} --interpretations CRIT_NC --object-names --include-patterns @${pattern_file} | tr '\n' ' ')
    # first check if there was any BAD comparison, if not, continue immediately
    [[ "${has_bad}" == "" && "${has_non_comparable}" == "" ]] && continue
    this_dir=${OUTDIR}/${d}
    # to the names that were returned, add a "$" at the end so that only exact matches are taken into account.
    # otherwise, other objects that only contain the names would also be considered.
    has_bad=$(make_exact_match_patterns ${has_bad})
    has_non_comparable=$(make_exact_match_patterns ${has_non_comparable})
    # get the objects that are BAD
    ${O2DPG_ROOT}/RelVal/o2dpg_release_validation.py inspect --path ${OUTDIR} --include-patterns ${has_bad} ${has_non_comparable} --output ${this_dir}
    # pipe the detector, path to the RelVal output and bad/non-comparable object names into the log file
    echo "${d};${this_dir};${has_bad};${has_non_comparable}" >> ${LOG_FILE}
done

exit 0
