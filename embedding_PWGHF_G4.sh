#!/bin/bash

# performing the PWGHF embedding example with G4 and 10 timeframes

cp ${O2DPG_ROOT}/MC/run/PWGHF/embedding_benchmark.sh .
SEED=624 NSIGEVENTS=100 NBKGEVENTS=100 NTIMEFRAMES=10 SIMENGINE=TGeant4 NWORKERS=8 JOBUTILS_SKIPDONE=ON JOBUTILS_KEEPJOBSCRIPT=ON ./embedding_benchmark.sh

MCRC=$?

return ${MCRC} 2> /dev/null || exit ${MCRC}
