#!/bin/bash

# make sure O2DPG + O2 is loaded
[ ! "${O2DPG_ROOT}" ] && echo "Error: This needs O2DPG loaded" && exit 1
[ ! "${O2_ROOT}" ] && echo "Error: This needs O2 loaded" && exit 1

# ----------- SETUP LOCAL CCDB CACHE --------------------------
export ALICEO2_CCDB_LOCALCACHE=$PWD/.ccdb

# ----------- START ACTUAL JOB  ----------------------------- 

NWORKERS=${NWORKERS:-8}
NTIMEFRAMES=${NTIMEFRAMES:-50} # set <= 50
INTRATE=${INTRATE:-10000}
NEVENTS=${NEVENTS:-50} # set <= 10000
MODULES="--skipModules ZDC"
SIMENGINE=${SIMENGINE:-TGeant4}

# download pre-generated events from alien
# there are 50 hepmc-files, with 10000 events per file
hepmc_local_file=events.hepmc
hepmc_events_dir="/alice/cern.ch/user/n/nburmaso/upcgen_events/dimuons_central"
for i in $( eval echo {1..$NTIMEFRAMES} )
do
  mkdir tf${i}
  alien.py cp alien:${hepmc_events_dir}/events.${i}.hepmc file:tf${i}/${hepmc_local_file}
done

# create workflow
${O2DPG_ROOT}/MC/bin/o2dpg_sim_workflow.py \
  -eCM 5020 \
  -col PbPb \
  -gen hepmc \
  -tf ${NTIMEFRAMES} \
  -ns ${NEVENTS} \
  -e ${SIMENGINE} \
  -j ${NWORKERS} \
  -interactionRate ${INTRATE} \
  -run 300000 \
  -confKey "HepMC.fileName=${hepmc_local_file};HepMC.version=3" \
  --include-qc --include-analysis \
  -productionTag "alibi_O2DPG_upc_test"

# run workflow
${O2DPG_ROOT}/MC/bin/o2_dpg_workflow_runner.py -f workflow.json -tt aod
MCRC=$?

if [ "${MCRC}" = "0" ]; then
  # publish the AODs to ALIEN
  copy_ALIEN "*AO2D*"
fi

# publish the original data to ALIEN
tar -czf mcarchive.tar.gz workflow.json tf* QC pipeline*
copy_ALIEN mcarchive.tar.gz

return ${MCRC} 2> /dev/null || exit ${MCRC}
