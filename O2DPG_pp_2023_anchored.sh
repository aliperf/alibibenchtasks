#!/bin/bash

#
# A example workflow MC->RECO->AOD for a simple pp min bias
# production, targetting test beam conditions.
#
# In addition, we target to exercise the whole chain of anchoring
# mechanisms

# example anchoring
# take run 536757 as it has a high CPU efficiency
export ALIEN_JDL_LPMANCHORPASSNAME=apass3
export ALIEN_JDL_MCANCHOR=apass3
export ALIEN_JDL_COLLISIONSYSTEM=p-p
export ALIEN_JDL_CPULIMIT=8
export ALIEN_JDL_LPMPASSNAME=apass3
export ALIEN_JDL_LPMRUNNUMBER=536757
export ALIEN_JDL_LPMPRODUCTIONTYPE=MC
export ALIEN_JDL_LPMINTERACTIONTYPE=pp
export ALIEN_JDL_LPMPRODUCTIONTAG=LHC23k4e
export ALIEN_JDL_LPMANCHORRUN=536757
export ALIEN_JDL_LPMANCHORPRODUCTION=LHC23s
export ALIEN_JDL_LPMANCHORYEAR=2023

# 5 TFs and 2000events/TF correspond to our unanchored MB test
export NTIMEFRAMES=5
export NSIGEVENTS=2000
export SPLITID=100
export PRODSPLIT=153
export CYCLE=0

# on the GRID, this is set, for our use case, we can mimic any job ID
export ALIEN_PROC_ID=2963436952

# run the central anchor steering script; this includes
# * derive timestamp
# * derive interaction rate
# * extract and prepare configurations (which detectors are contained in the run etc.)
# * run the simulation (and QC)
${O2DPG_ROOT}/MC/run/ANCHOR/anchorMC.sh

# cache the return code
MCRC=$?

if [ "${MCRC}" != "0" ]; then

  # QC was done in anchor, only check if successful
  err_logs=$(get_error_logs $(pwd) --include-grep "QC")
  [ "${err_logs}" != "" ] && send_mattermost "--text QC stage **failed** :x: --files ${err_logs}" || send_mattermost "--text QC **passed** :white_check_mark:"
fi

exit ${MCRC}
