#!/bin/bash

# This tests checks if CCDB objects that have been downloaded as local snapshot
# are compatible with a particular timestamp.

# The the test will walk across all test directories one level higher and
# a) determine the timestamp from the O2DPG workflow file
# b) scan all objects in the local .ccdb snapshot directory which was created as part of the test

# By itself this test is useless. It should be run as last step after other processing tasks
# have finished. It also assumes that it is run within the alibi testing setup.


# firstly find all directories to check
CCDBDIRS=`find ../ -type d -wholename "*/.ccdb"`

# iterate over all directories; extract timestamp and check objects
FINAL_RC=0 # final return code
for d in ${CCDBDIRS}; do
  echo "Inspecting directory ${d}"
  # extract timestamp from workflow.json file
  WORKFLOWFILE=${d}/../workflow.json
  if [ -f ${WORKFLOWFILE} ]; then
    TIMESTAMP=$(grep "\-\-timestamp" ${WORKFLOWFILE}  | sed 's/.*timestamp//' | awk '//{print $1}' | sort | uniq)
    if [ "${TIMESTAMP}" ]; then
      echo "Obtained timestamp ${TIMESTAMP}" 
      # now run the check on all CCDB files in the snapshot directory
      o2-ccdb-inspectccdbfile -f `find ${d} -name "snapshot.root"` -t ${TIMESTAMP} &>> CCDBcheckLog.log
      RC=$?
      if [ "${RC}" != "0" ]; then
        echo "Check of snapshot ${d} **failed**"
      fi
      let FINAL_RC=FINAL_RC+RC
    fi
  else 
    echo "No workflow.json file found. Not extracting a timestamp and not checking CCDB."
  fi
done

if [ "${FINAL_RC}" != "0" ]; then
  send_mattermost "--text CCDB snapshot checks **failed** :x: --files CCDBcheckLog.log"
else
  send_mattermost "--text CCDB snapshot checks **succeeded**"
fi

exit ${FINAL_RC}
