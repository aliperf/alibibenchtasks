#!/bin/bash

# A housekeeping task to avoid filling the disc.
# This task removes benchmark data older than 7 days.

[[ "${ALIPERF_DATA_DIR}" == "" ]] && echo "ALIPERF_DATA_DIR not set" && (return 1 2>/dev/null || exit 1)

cd ${ALIPERF_DATA_DIR}
# search all folder starting with a date ... older than 14 days and remove
find . -maxdepth 1 -type d -regextype sed -regex "^\./[0-9]\{2\}-[0-9]\{2\}-[0-9]\{4\}.*" -mtime +14 -type d -exec rm -rf {} +

return 0 2>/dev/null || exit 0
