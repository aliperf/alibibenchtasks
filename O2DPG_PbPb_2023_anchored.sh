#!/bin/bash

#
# A example workflow MC->RECO->AOD for a simple pp min bias
# production, targetting test beam conditions.
#
# In addition, we target to exercise the whole chain of anchoring
# mechanisms

# Run the test from O2DPG
${O2DPG_ROOT}/MC/run/ANCHOR/tests/test_anchor_2023_apass2_PbPb.sh

# cache the return code
MCRC=$?

if [ "${MCRC}" != "0" ]; then

  # QC was done in anchor, only check if successful
  err_logs=$(get_error_logs $(pwd) --include-grep "QC")
  [ "${err_logs}" != "" ] && send_mattermost "--text QC stage **failed** :x: --files ${err_logs}" || send_mattermost "--text QC **passed** :white_check_mark:"
fi

# publish the original data to ALIEN
tar -czf mcarchive.tar.gz workflow.json tf* QC pipeline*
copy_ALIEN mcarchive.tar.gz

exit ${MCRC}
